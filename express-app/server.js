const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const path = require('path');
const cors = require('cors');
const os = require('os');

const businessRoute = require('./routes/business.route');
const config = require('./DB').DB;

console.log(config);

mongoose.Promise = global.Promise;

mongoose.connect(config, { 
  useNewUrlParser: true 
}).then(() => {
  console.log('Database is connected')
}, err => { 
  console.log('Can not connect to the database'+ err)
});

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use('/business', businessRoute);

const port = process.env.PORT || 3000;

const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});